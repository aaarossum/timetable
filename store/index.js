// import Vue from 'vue'
// =================================================
// State
// =================================================
export const state = () => {
  const s = {
    options: {
      favourites: false,
      text: false,
      short: false,
      reverse: false,
      hour24: true,
      startTime: true,
      endTime: true,
      stage: true
    },
    currentView: 'day',
    optionDescr: [
      { id: 'favourites', descr: 'Show Favourites Only', table: true },
      { id: 'text', descr: 'Show Plain Text', table: false },
      { id: 'short', descr: 'Use Short Names', table: true },
      { id: 'reverse', descr: 'Reverse Order', table: true },
      { id: 'hour24', descr: 'Show 24-Hour Time Format', table: true },
      { id: 'startTime', descr: 'Show Start Time', table: false },
      { id: 'endTime', descr: 'Show End Time', table: false },
      { id: 'stage', descr: 'Show Stage', table: false }
    ],
    viewDescr: [
      { id: 'day', descr: 'Day View' },
      { id: 'stage', descr: 'Group By Stage' },
      { id: 'table', descr: 'Table View' }
    ],
    festivals: [
      {
        name: 'Creamfields',
        timetable: [
          {
            day: 'Thursday',
            start: 17,
            end: 23,
            events: [
              {
                stage: 'Axtone',
                start: '22:00',
                end: '23:00',
                artists: ['Kryder'],
                separator: ''
              },
              {
                stage: 'Axtone',
                start: '21:00',
                end: '22:00',
                artists: ['Chocolate Puma'],
                separator: ''
              },
              {
                stage: 'Axtone',
                start: '20:00',
                end: '21:00',
                artists: ['D.O.D'],
                separator: ''
              },
              {
                stage: 'Axtone',
                start: '19:00',
                end: '20:00',
                artists: ['Tom Staar'],
                separator: ''
              },
              {
                stage: 'Axtone',
                start: '18:00',
                end: '19:00',
                artists: ['Magnifience'],
                separator: ''
              },
              {
                stage: 'Axtone',
                start: '17:00',
                end: '18:00',
                artists: ['Jack Wins'],
                separator: ''
              },
              {
                stage: 'Pepsi Max Arena',
                start: '22:00',
                end: '23:00',
                artists: ['Greg Downey'],
                separator: ''
              },
              {
                stage: 'Pepsi Max Arena',
                start: '21:00',
                end: '22:00',
                artists: ['Shugz vs David Rust'],
                separator: ''
              },
              {
                stage: 'Pepsi Max Arena',
                start: '20:00',
                end: '21:00',
                artists: ['Menno De Jong'],
                separator: ''
              },
              {
                stage: 'Pepsi Max Arena',
                start: '19:00',
                end: '20:00',
                artists: ['Standerwick'],
                separator: ''
              },
              {
                stage: 'Pepsi Max Arena',
                start: '18:00',
                end: '19:00',
                artists: ['Ashley Wallbridge'],
                separator: ''
              },
              {
                stage: 'Pepsi Max Arena',
                start: '17:00',
                end: '18:00',
                artists: ['The Stupid Experts'],
                separator: ''
              },
              {
                stage: 'Sexy At Nature',
                start: '21:00',
                end: '23:00',
                artists: ['Sunnery James & Ryan Marciano'],
                separator: ''
              },
              {
                stage: 'Sexy At Nature',
                start: '20:00',
                end: '21:00',
                artists: ['Roger Sanchez'],
                separator: ''
              },
              {
                stage: 'Sexy At Nature',
                start: '19:00',
                end: '20:00',
                artists: ['Cedric Gervais'],
                separator: ''
              },
              {
                stage: 'Sexy At Nature',
                start: '18:00',
                end: '19:00',
                artists: ['Mia More'],
                separator: ''
              },
              {
                stage: 'Sexy At Nature',
                start: '17:00',
                end: '18:00',
                artists: ['Lilrockit'],
                separator: ''
              },
              {
                stage: 'Warehouse',
                start: '22:00',
                end: '23:00',
                artists: ['Deniz Koyu'],
                separator: ''
              },
              {
                stage: 'Warehouse',
                start: '21:10',
                end: '22:00',
                artists: ['Sick Indviduals'],
                separator: ''
              },
              {
                stage: 'Warehouse',
                start: '20:20',
                end: '21:10',
                artists: ['Thomas Gold'],
                separator: ''
              },
              {
                stage: 'Warehouse',
                start: '19:30',
                end: '20:20',
                artists: ['Corey James'],
                separator: ''
              },
              {
                stage: 'Warehouse',
                start: '18:40',
                end: '19:30',
                artists: ['Matt Nash'],
                separator: ''
              },
              {
                stage: 'Warehouse',
                start: '17:50',
                end: '18:40',
                artists: ['Will K'],
                separator: ''
              },
              {
                stage: 'Warehouse',
                start: '17:00',
                end: '17:50',
                artists: ['Junior J'],
                separator: ''
              }
            ]
          },
          {
            day: 'Friday',
            start: 15,
            end: 23,
            events: [
              {
                stage: 'Arc',
                start: '21:30',
                end: '23:00',
                artists: ['Deadmau5'],
                separator: ''
              },
              {
                stage: 'Arc',
                start: '21:00',
                end: '21:30',
                artists: ['Change Over'],
                separator: ''
              },
              {
                stage: 'Arc',
                start: '20:00',
                end: '21:00',
                artists: ['Tchami'],
                separator: ''
              },
              {
                stage: 'Arc',
                start: '18:30',
                end: '20:00',
                artists: ['Fatboy Slim'],
                separator: ''
              },
              {
                stage: 'Arc',
                start: '18:00',
                end: '18:30',
                artists: ['Tom Williams'],
                separator: ''
              },
              {
                stage: 'Freakshow',
                start: '22:00',
                end: '23:00',
                artists: ['Sub Zero Project'],
                separator: ''
              },
              {
                stage: 'Freakshow',
                start: '20:45',
                end: '22:00',
                artists: ['Will Sparks'],
                separator: ''
              },
              {
                stage: 'Freakshow',
                start: '19:30',
                end: '20:45',
                artists: ['Ben Nicky'],
                separator: ''
              },
              {
                stage: 'Freakshow',
                start: '18:15',
                end: '19:30',
                artists: ['Timmy Trumpet'],
                separator: ''
              },
              {
                stage: 'Freakshow',
                start: '17:00',
                end: '18:15',
                artists: ['Marlo'],
                separator: ''
              },
              {
                stage: 'Freakshow',
                start: '16:00',
                end: '17:00',
                artists: ['Teddy Cream'],
                separator: ''
              },
              {
                stage: 'Freakshow',
                start: '15:00',
                end: '16:00',
                artists: ['Dr Phunk'],
                separator: ''
              },
              {
                stage: 'Silo',
                start: '21:00',
                end: '23:00',
                artists: ['Amelie Lens'],
                separator: ''
              },
              {
                stage: 'Silo',
                start: '19:30',
                end: '21:00',
                artists: ['Len Faki'],
                separator: ''
              },
              {
                stage: 'Silo',
                start: '18:00',
                end: '19:30',
                artists: ['Joyhauser'],
                separator: ''
              },
              {
                stage: 'Silo',
                start: '16:30',
                end: '18:00',
                artists: ['Marcel Dettmann'],
                separator: ''
              },
              {
                stage: 'Silo',
                start: '15:00',
                end: '16:30',
                artists: ['Rohar'],
                separator: ''
              },
              {
                stage: 'Generator',
                start: '22:15',
                end: '23:00',
                artists: ['Holy Goof'],
                separator: ''
              },
              {
                stage: 'Generator',
                start: '21:30',
                end: '22:15',
                artists: ['Jauz'],
                separator: ''
              },
              {
                stage: 'Generator',
                start: '20:40',
                end: '21:30',
                artists: ['Dyro'],
                separator: ''
              },
              {
                stage: 'Generator',
                start: '19:35',
                end: '20:40',
                artists: ['TV Noise'],
                separator: ''
              },
              {
                stage: 'Generator',
                start: '19:30',
                end: '19:35',
                artists: ['Change Over'],
                separator: ''
              },
              {
                stage: 'Generator',
                start: '18:00',
                end: '19:30',
                artists: ['Martin Garrix'],
                separator: ''
              },
              {
                stage: 'Generator',
                start: '17:35',
                end: '18:00',
                artists: ['Change Over'],
                separator: ''
              },
              {
                stage: 'Generator',
                start: '16:30',
                end: '17:35',
                artists: ['Malaa'],
                separator: ''
              },
              {
                stage: 'Generator',
                start: '15:30',
                end: '16:30',
                artists: ['Bart B More'],
                separator: ''
              },
              {
                stage: 'Generator',
                start: '15:00',
                end: '15:30',
                artists: ['Ed Mackie'],
                separator: ''
              },
              {
                stage: 'Steel Yard',
                start: '21:00',
                end: '23:00',
                artists: ['Eric Prydz Void'],
                separator: ''
              },
              {
                stage: 'Steel Yard',
                start: '20:30',
                end: '21:00',
                artists: ['Change Over'],
                separator: ''
              },
              {
                stage: 'Steel Yard',
                start: '19:00',
                end: '20:30',
                artists: ['Solardo'],
                separator: ''
              },
              {
                stage: 'Steel Yard',
                start: '17:30',
                end: '19:00',
                artists: ['Dusky'],
                separator: ''
              },
              {
                stage: 'Steel Yard',
                start: '16:00',
                end: '17:30',
                artists: ['Ejeca'],
                separator: ''
              },
              {
                stage: 'Steel Yard',
                start: '15:00',
                end: '16:00',
                artists: ['Phonix'],
                separator: ''
              },
              {
                stage: 'Warehouse',
                start: '22:00',
                end: '23:00',
                artists: ['Jamie Jones', 'Green Velvet', 'Patrick Topping'],
                separator: ' b2b '
              },
              {
                stage: 'Warehouse',
                start: '20:30',
                end: '22:00',
                artists: ['Jamie Jones'],
                separator: ''
              },
              {
                stage: 'Warehouse',
                start: '19:30',
                end: '20:30',
                artists: ['Green Velvet'],
                separator: ''
              },
              {
                stage: 'Warehouse',
                start: '18:30',
                end: '19:30',
                artists: ['Patrick Topping'],
                separator: ''
              },
              {
                stage: 'Warehouse',
                start: '16:30',
                end: '18:30',
                artists: ['Richy Ahmed', 'Darius Syrossian'],
                separator: ' b2b '
              },
              {
                stage: 'Warehouse',
                start: '15:00',
                end: '16:30',
                artists: ['Lauren Lo Sung'],
                separator: ''
              },
              {
                stage: 'Andy C Presents',
                start: '22:00',
                end: '23:00',
                artists: ['Dimension'],
                separator: ''
              },
              {
                stage: 'Andy C Presents',
                start: '20:30',
                end: '22:00',
                artists: ['Andy C'],
                separator: ''
              },
              {
                stage: 'Andy C Presents',
                start: '19:30',
                end: '20:30',
                artists: ['Shy FX'],
                separator: ''
              },
              {
                stage: 'Andy C Presents',
                start: '18:30',
                end: '19:30',
                artists: ['Culture Shock'],
                separator: ''
              },
              {
                stage: 'Andy C Presents',
                start: '17:30',
                end: '18:30',
                artists: ['Randall'],
                separator: ''
              },
              {
                stage: 'Andy C Presents',
                start: '16:30',
                end: '17:30',
                artists: ['Upgrade', 'Limited'],
                separator: ' b2b '
              },
              {
                stage: 'Andy C Presents',
                start: '15:45',
                end: '16:30',
                artists: ['Harriet Jaxxon'],
                separator: ''
              },
              {
                stage: 'Andy C Presents',
                start: '15:00',
                end: '15:45',
                artists: ['Magnetude'],
                separator: ''
              },
              {
                stage: 'Pepsi Max Arena',
                start: '21:45',
                end: '23:00',
                artists: ['John O’Callaghan'],
                separator: ''
              },
              {
                stage: 'Pepsi Max Arena',
                start: '20:45',
                end: '21:45',
                artists: ['Orjan Nilsen'],
                separator: ''
              },
              {
                stage: 'Pepsi Max Arena',
                start: '19:15',
                end: '20:45',
                artists: ['Ferry Corsten'],
                separator: ''
              },
              {
                stage: 'Pepsi Max Arena',
                start: '18:00',
                end: '19:15',
                artists: ['Eddie Halliwell'],
                separator: ''
              },
              {
                stage: 'Pepsi Max Arena',
                start: '17:00',
                end: '18:00',
                artists: ['Ruben De Ronde'],
                separator: ''
              },
              {
                stage: 'Pepsi Max Arena',
                start: '16:00',
                end: '17:00',
                artists: ['Ciaran Mcauley'],
                separator: ''
              },
              {
                stage: 'Pepsi Max Arena',
                start: '15:00',
                end: '16:00',
                artists: ['Daxson'],
                separator: ''
              },
              {
                stage: 'The Courtyard',
                start: '22:00',
                end: '23:00',
                artists: ['Radical Redemption'],
                separator: ''
              },
              {
                stage: 'The Courtyard',
                start: '21:00',
                end: '22:00',
                artists: ['Da Tweekaz'],
                separator: ''
              },
              {
                stage: 'The Courtyard',
                start: '20:00',
                end: '21:00',
                artists: ['Headhunterz'],
                separator: ''
              },
              {
                stage: 'The Courtyard',
                start: '19:00',
                end: '20:00',
                artists: ['Brennan Heart'],
                separator: ''
              },
              {
                stage: 'The Courtyard',
                start: '18:00',
                end: '19:00',
                artists: ['Alex Kidd'],
                separator: ''
              },
              {
                stage: 'The Courtyard',
                start: '17:00',
                end: '18:00',
                artists: ['Andy Whitby'],
                separator: ''
              },
              {
                stage: 'The Courtyard',
                start: '15:00',
                end: '17:00',
                artists: ['GGXH Residents Phil Mackintosh', 'Reklus & Shaun T'],
                separator: ', '
              },
              {
                stage: 'Utilita Power Tree',
                start: '21:00',
                end: '23:00',
                artists: ['Jaguar Skills'],
                separator: ''
              },
              {
                stage: 'Utilita Power Tree',
                start: '19:30',
                end: '21:00',
                artists: ['Prok & Fitch'],
                separator: ''
              },
              {
                stage: 'Utilita Power Tree',
                start: '18:00',
                end: '19:30',
                artists: ['Blueprint'],
                separator: ''
              },
              {
                stage: 'Utilita Power Tree',
                start: '16:30',
                end: '18:00',
                artists: ['Paul Nunn & James Glover'],
                separator: ''
              },
              {
                stage: 'Utilita Power Tree',
                start: '15:00',
                end: '16:30',
                artists: ['Daniel Frank & Jamie Grounds'],
                separator: ''
              }
            ]
          },
          {
            day: 'Saturday',
            start: 14,
            end: 28,
            events: [
              {
                stage: 'Arc',
                start: '21:30',
                end: '23:00',
                artists: ['Calvin Harris'],
                separator: ''
              },
              {
                stage: 'Arc',
                start: '21:00',
                end: '21:30',
                artists: ['Change Over'],
                separator: ''
              },
              {
                stage: 'Arc',
                start: '19:30',
                end: '21:00',
                artists: ['MK'],
                separator: ''
              },
              {
                stage: 'Arc',
                start: '18:00',
                end: '19:30',
                artists: ['Duke Dumont'],
                separator: ''
              },
              {
                stage: 'Arc',
                start: '16:30',
                end: '18:00',
                artists: ['Franky Wah'],
                separator: ''
              },
              {
                stage: 'Arc',
                start: '15:00',
                end: '16:30',
                artists: ['Generik'],
                separator: ''
              },
              {
                stage: 'Arc',
                start: '14:00',
                end: '15:00',
                artists: ['Gareth Wyn'],
                separator: ''
              },
              {
                stage: 'BBC Radio 1 Stage',
                start: '21:30',
                end: '23:00',
                artists: ['The Chemical Brothers'],
                separator: ''
              },
              {
                stage: 'BBC Radio 1 Stage',
                start: '21:00',
                end: '21:30',
                artists: ['Change Over'],
                separator: ''
              },
              {
                stage: 'BBC Radio 1 Stage',
                start: '19:30',
                end: '21:00',
                artists: ['Annie Mac'],
                separator: ''
              },
              {
                stage: 'BBC Radio 1 Stage',
                start: '18:00',
                end: '19:30',
                artists: ['Denis Sulta'],
                separator: ''
              },
              {
                stage: 'BBC Radio 1 Stage',
                start: '16:30',
                end: '18:00',
                artists: ['Mistajam'],
                separator: ''
              },
              {
                stage: 'BBC Radio 1 Stage',
                start: '15:00',
                end: '16:30',
                artists: ['The Lovely Laura & Ben Santiago'],
                separator: ''
              },
              {
                stage: 'BBC Radio 1 Stage',
                start: '14:00',
                end: '15:00',
                artists: ['James Organ'],
                separator: ''
              },
              {
                stage: 'Steel Yard',
                start: '02:00',
                end: '04:00',
                artists: ['Nicole Moudaber'],
                separator: ''
              },
              {
                stage: 'Steel Yard',
                start: '00:00',
                end: '02:00',
                artists: ['Carl Cox'],
                separator: ''
              },
              {
                stage: 'Steel Yard',
                start: '22:00',
                end: '00:00',
                artists: ['Pan-Pot'],
                separator: ''
              },
              {
                stage: 'Steel Yard',
                start: '20:30',
                end: '22:00',
                artists: ['Umek'],
                separator: ''
              },
              {
                stage: 'Steel Yard',
                start: '19:00',
                end: '20:30',
                artists: ['Cristian Varela'],
                separator: ''
              },
              {
                stage: 'Steel Yard',
                start: '17:30',
                end: '19:00',
                artists: ['Jon Rundell'],
                separator: ''
              },
              {
                stage: 'Steel Yard',
                start: '16:00',
                end: '17:30',
                artists: ['Sidney Charles'],
                separator: ''
              },
              {
                stage: 'Steel Yard',
                start: '15:00',
                end: '16:00',
                artists: ['Saytex'],
                separator: ''
              },
              {
                stage: 'Steel Yard',
                start: '14:00',
                end: '15:00',
                artists: ['Christopher Coe'],
                separator: ''
              },
              {
                stage: 'Generator',
                start: '03:05',
                end: '04:00',
                artists: ['Gallya'],
                separator: ''
              },
              {
                stage: 'Generator',
                start: '02:05',
                end: '03:05',
                artists: ['I_O'],
                separator: ''
              },
              {
                stage: 'Generator',
                start: '00:05',
                end: '02:04',
                artists: ['Testpilot'],
                separator: ''
              },
              {
                stage: 'Generator',
                start: '00:00',
                end: '00:05',
                artists: ['Chang Over'],
                separator: ''
              },
              {
                stage: 'Generator',
                start: '23:00',
                end: '00:00',
                artists: ['Rezz'],
                separator: ''
              },
              {
                stage: 'Generator',
                start: '22:00',
                end: '23:00',
                artists: ['Rinzen'],
                separator: ''
              },
              {
                stage: 'Generator',
                start: '21:00',
                end: '22:00',
                artists: ['Attlas'],
                separator: ''
              },
              {
                stage: 'Generator',
                start: '20:00',
                end: '21:00',
                artists: ['Feed Me'],
                separator: ''
              },
              {
                stage: 'Generator',
                start: '19:00',
                end: '20:00',
                artists: ['Callie Reiff'],
                separator: ''
              },
              {
                stage: 'Generator',
                start: '18:00',
                end: '19:00',
                artists: ['Livsey'],
                separator: ''
              },
              {
                stage: 'Generator',
                start: '17:00',
                end: '18:00',
                artists: ['Jay Robinson'],
                separator: ''
              },
              {
                stage: 'Generator',
                start: '16:00',
                end: '17:00',
                artists: ['Killa HZ'],
                separator: ''
              },
              {
                stage: 'Generator',
                start: '15:00',
                end: '16:00',
                artists: ['Goooey Vuitton'],
                separator: ''
              },
              {
                stage: 'Generator',
                start: '14:00',
                end: '15:00',
                artists: ['Ami Carmine'],
                separator: ''
              },
              {
                stage: 'AnjunaBeats',
                start: '02:30',
                end: '04:00',
                artists: ['Genix'],
                separator: ''
              },
              {
                stage: 'AnjunaBeats',
                start: '01:00',
                end: '02:30',
                artists: ['Ilan Bluestone'],
                separator: ''
              },
              {
                stage: 'AnjunaBeats',
                start: '23:30',
                end: '01:00',
                artists: ['Above & Beyond'],
                separator: ''
              },
              {
                stage: 'AnjunaBeats',
                start: '22:00',
                end: '23:30',
                artists: ['Spencer Brown'],
                separator: ''
              },
              {
                stage: 'AnjunaBeats',
                start: '20:30',
                end: '22:00',
                artists: ['Gabriel & Dresden'],
                separator: ''
              },
              {
                stage: 'AnjunaBeats',
                start: '19:00',
                end: '20:30',
                artists: ['Grum'],
                separator: ''
              },
              {
                stage: 'AnjunaBeats',
                start: '17:30',
                end: '19:00',
                artists: ['Oliver Smit'],
                separator: ''
              },
              {
                stage: 'AnjunaBeats',
                start: '16:00',
                end: '17:30',
                artists: ['Tinlicker'],
                separator: ''
              },
              {
                stage: 'AnjunaBeats',
                start: '14:00',
                end: '16:00',
                artists: ['Amy Wiles'],
                separator: ''
              },
              {
                stage: 'Warehouse',
                start: '02:30',
                end: '04:00',
                artists: ['The Black Madonna'],
                separator: ''
              },
              {
                stage: 'Warehouse',
                start: '01:00',
                end: '02:30',
                artists: ['Peggy Gou'],
                separator: ''
              },
              {
                stage: 'Warehouse',
                start: '23:00',
                end: '01:00',
                artists: ['Pete Tong'],
                separator: ''
              },
              {
                stage: 'Warehouse',
                start: '21:30',
                end: '23:00',
                artists: ['Bicep'],
                separator: ''
              },
              {
                stage: 'Warehouse',
                start: '20:00',
                end: '21:30',
                artists: ['Hot Since 82'],
                separator: ''
              },
              {
                stage: 'Warehouse',
                start: '18:30',
                end: '20:00',
                artists: ['La Fleur'],
                separator: ''
              },
              {
                stage: 'Warehouse',
                start: '17:00',
                end: '16:30',
                artists: ['Athea'],
                separator: ''
              },
              {
                stage: 'Warehouse',
                start: '15:30',
                end: '17:00',
                artists: ['Joris Voorn'],
                separator: ''
              },
              {
                stage: 'Warehouse',
                start: '14:00',
                end: '15:30',
                artists: ['Tom Williams & Josh Samuel'],
                separator: ''
              },
              {
                stage: 'Sub Aural',
                start: '01:30',
                end: '04:00',
                artists: ['Crucast'],
                separator: ''
              },
              {
                stage: 'Sub Aural',
                start: '00:00',
                end: '01:30',
                artists: ['Chase & Status'],
                separator: ''
              },
              {
                stage: 'Sub Aural',
                start: '23:00',
                end: '00:00',
                artists: ['Wilkinson'],
                separator: ''
              },
              {
                stage: 'Sub Aural',
                start: '22:00',
                end: '23:00',
                artists: ['High Contest'],
                separator: ''
              },
              {
                stage: 'Sub Aural',
                start: '21:00',
                end: '22:00',
                artists: ['Matrix & Futurebound'],
                separator: ''
              },
              {
                stage: 'Sub Aural',
                start: '20:00',
                end: '21:00',
                artists: ['North Base'],
                separator: ''
              },
              {
                stage: 'Sub Aural',
                start: '19:00',
                end: '20:00',
                artists: ['Yako (featuring Bridge)'],
                separator: ''
              },
              {
                stage: 'Sub Aural',
                start: '18:00',
                end: '19:00',
                artists: ['Understate'],
                separator: ''
              },
              {
                stage: 'Sub Aural',
                start: '17:00',
                end: '18:00',
                artists: ['Andy Mac'],
                separator: ''
              },
              {
                stage: 'Sub Aural',
                start: '16:00',
                end: '17:00',
                artists: ['Stet'],
                separator: ''
              },
              {
                stage: 'Sub Aural',
                start: '15:00',
                end: '16:00',
                artists: ['Darren Donnelly & Jae Holmes'],
                separator: ''
              },
              {
                stage: 'Silo',
                start: '03:00',
                end: '04:00',
                artists: ['TNT'],
                separator: ''
              },
              {
                stage: 'Silo',
                start: '02:00',
                end: '03:00',
                artists: ['NoiseControllers'],
                separator: ''
              },
              {
                stage: 'Silo',
                start: '01:00',
                end: '02:00',
                artists: ['Blastoyz'],
                separator: ''
              },
              {
                stage: 'Silo',
                start: '00:00',
                end: '01:00',
                artists: ['Nicky Romero'],
                separator: ''
              },
              {
                stage: 'Silo',
                start: '23:00',
                end: '00:00',
                artists: ['Afrojack'],
                separator: ''
              },
              {
                stage: 'Silo',
                start: '22:00',
                end: '23:00',
                artists: ['Dimitri Vangelis & Wyman'],
                separator: ''
              },
              {
                stage: 'Silo',
                start: '21:00',
                end: '22:00',
                artists: ['Maurice West'],
                separator: ''
              },
              {
                stage: 'Silo',
                start: '20:00',
                end: '21:00',
                artists: ['Jack Eye Jones'],
                separator: ''
              },
              {
                stage: 'Silo',
                start: '18:30',
                end: '20:00',
                artists: ['Nick Coulson'],
                separator: ''
              },
              {
                stage: 'Silo',
                start: '17:00',
                end: '18:30',
                artists: ['Hardman & Devall'],
                separator: ''
              },
              {
                stage: 'Silo',
                start: '15:00',
                end: '17:00',
                artists: ['Skram'],
                separator: ''
              },
              {
                stage: 'Pepsi Max Arena',
                start: '02:15',
                end: '04:00',
                artists: ['Benny Benassi'],
                separator: ''
              },
              {
                stage: 'Pepsi Max Arena',
                start: '01:15',
                end: '02:15',
                artists: ['Dillon Francis'],
                separator: ''
              },
              {
                stage: 'Pepsi Max Arena',
                start: '01:00',
                end: '01:15',
                artists: ['Change Over'],
                separator: ''
              },
              {
                stage: 'Pepsi Max Arena',
                start: '23:30',
                end: '01:00',
                artists: ['Laidback Luke'],
                separator: ''
              },
              {
                stage: 'Pepsi Max Arena',
                start: '22:00',
                end: '23:30',
                artists: ['Ed Mackie'],
                separator: ''
              },
              {
                stage: 'Pepsi Max Arena',
                start: '20:30',
                end: '20:00',
                artists: ['Third Party'],
                separator: ''
              },
              {
                stage: 'Pepsi Max Arena',
                start: '19:00',
                end: '20:30',
                artists: ['Bassjackers'],
                separator: ''
              },
              {
                stage: 'Pepsi Max Arena',
                start: '17:30',
                end: '19:00',
                artists: ['Mattn'],
                separator: ''
              },
              {
                stage: 'Pepsi Max Arena',
                start: '16:30',
                end: '17:30',
                artists: ['Jack Eye Jones'],
                separator: ''
              },
              {
                stage: 'Pepsi Max Arena',
                start: '15:30',
                end: '16:30',
                artists: ['Paul Nunn & James Glover'],
                separator: ''
              },
              {
                stage: 'Pepsi Max Arena',
                start: '14:00',
                end: '15:30',
                artists: ['Billie Clements & Chris Wright'],
                separator: ''
              },
              {
                stage: 'The Courtyard',
                start: '22:00',
                end: '23:00',
                artists: ['Will Atkinson'],
                separator: ''
              },
              {
                stage: 'The Courtyard',
                start: '21:00',
                end: '22:00',
                artists: ['John Askew'],
                separator: ''
              },
              {
                stage: 'The Courtyard',
                start: '19:45',
                end: '21:00',
                artists: ['Bryan Kearney'],
                separator: ''
              },
              {
                stage: 'The Courtyard',
                start: '18:15',
                end: '19:45',
                artists: ['Gareth Emery'],
                separator: ''
              },
              {
                stage: 'The Courtyard',
                start: '17:00',
                end: '18:15',
                artists: ['Estiva'],
                separator: ''
              },
              {
                stage: 'The Courtyard',
                start: '16:00',
                end: '17:00',
                artists: ['Maura Picotto'],
                separator: ''
              },
              {
                stage: 'The Courtyard',
                start: '15:00',
                end: '16:00',
                artists: ['Liam Wilson'],
                separator: ''
              },
              {
                stage: 'The Courtyard',
                start: '14:00',
                end: '15:00',
                artists: ['Pete Bromage', 'Jamie Cooper & B.Viss'],
                separator: ', '
              },
              {
                stage: 'Utilita Power Tree',
                start: '21:30',
                end: '23:00',
                artists: ['Babalou & Sosa'],
                separator: ''
              },
              {
                stage: 'Utilita Power Tree',
                start: '20:00',
                end: '21:30',
                artists: ['FOOR'],
                separator: ''
              },
              {
                stage: 'Utilita Power Tree',
                start: '18:30',
                end: '20:00',
                artists: ['Nathan Dawe'],
                separator: ''
              },
              {
                stage: 'Utilita Power Tree',
                start: '17:30',
                end: '18:30',
                artists: ['Cal Griffin'],
                separator: ''
              },
              {
                stage: 'Utilita Power Tree',
                start: '16:30',
                end: '17:30',
                artists: ['Mitch Nunn'],
                separator: ''
              },
              {
                stage: 'Utilita Power Tree',
                start: '15:30',
                end: '16:30',
                artists: ['Simon Cousens'],
                separator: ''
              },
              {
                stage: 'Utilita Power Tree',
                start: '14:00',
                end: '15:30',
                artists: ['Au'],
                separator: ''
              }
            ]
          },
          {
            day: 'Sunday',
            start: 12,
            end: 23,
            events: [
              {
                stage: 'Arc',
                start: '21:30',
                end: '23:00',
                artists: ['Swedish House Mafia'],
                separator: ''
              },
              {
                stage: 'Horizon',
                start: '21:30',
                end: '23:00',
                artists: ['Tiesto'],
                separator: ''
              },
              {
                stage: 'Horizon',
                start: '21:00',
                end: '21:30',
                artists: ['Change Over'],
                separator: ''
              },
              {
                stage: 'Horizon',
                start: '19:30',
                end: '21:00',
                artists: ['Dimitri Vegas & Like Mike'],
                separator: ''
              },
              {
                stage: 'Horizon',
                start: '18:30',
                end: '19:30',
                artists: ['Church Service'],
                separator: ''
              },
              {
                stage: 'Horizon',
                start: '17:00',
                end: '18:30',
                artists: ['Oliver Heldens'],
                separator: ''
              },
              {
                stage: 'Horizon',
                start: '15:30',
                end: '17:00',
                artists: ['Faithless DJ Set'],
                separator: ''
              },
              {
                stage: 'Horizon',
                start: '14:00',
                end: '15:30',
                artists: ['Sunnery James & Ryan Marciano'],
                separator: ''
              },
              {
                stage: 'Horizon',
                start: '12:00',
                end: '14:00',
                artists: ['Carta'],
                separator: ''
              },
              {
                stage: 'Steel Yard',
                start: '21:00',
                end: '23:00',
                artists: ['Adam Beyer', 'Cirez D'],
                separator: ' x '
              },
              {
                stage: 'Steel Yard',
                start: '20:30',
                end: '21:00',
                artists: ['Change Over'],
                separator: ''
              },
              {
                stage: 'Steel Yard',
                start: '19:00',
                end: '20:30',
                artists: ['Kolsch'],
                separator: ''
              },
              {
                stage: 'Steel Yard',
                start: '17:30',
                end: '19:00',
                artists: ['Cristoph'],
                separator: ''
              },
              {
                stage: 'Steel Yard',
                start: '16:00',
                end: '17:30',
                artists: ['Enrico Sangiuliano'],
                separator: ''
              },
              {
                stage: 'Steel Yard',
                start: '14:30',
                end: '16:00',
                artists: ['B.Traits'],
                separator: ''
              },
              {
                stage: 'Steel Yard',
                start: '13:00',
                end: '14:30',
                artists: ['Athea'],
                separator: ''
              },
              {
                stage: 'Steel Yard',
                start: '12:00',
                end: '13:00',
                artists: ['George Last'],
                separator: ''
              },
              {
                stage: 'Generator',
                start: '21:00',
                end: '23:00',
                artists: ['Camelphat'],
                separator: ''
              },
              {
                stage: 'Generator',
                start: '19:30',
                end: '21:00',
                artists: ['Fisher'],
                separator: ''
              },
              {
                stage: 'Generator',
                start: '18:00',
                end: '19:30',
                artists: ['Paul Woolford'],
                separator: ''
              },
              {
                stage: 'Generator',
                start: '16:30',
                end: '18:00',
                artists: ['Mele'],
                separator: ''
              },
              {
                stage: 'Generator',
                start: '15:00',
                end: '16:30',
                artists: ['Heidi'],
                separator: ''
              },
              {
                stage: 'Generator',
                start: '13:30',
                end: '15:00',
                artists: ['Mason Maynard'],
                separator: ''
              },
              {
                stage: 'Generator',
                start: '12:00',
                end: '13:30',
                artists: ['Anthony Probyn'],
                separator: ''
              },
              {
                stage: 'Warehouse',
                start: '21:30',
                end: '23:00',
                artists: ['Alan Fitzpatrick'],
                separator: ''
              },
              {
                stage: 'Warehouse',
                start: '20:00',
                end: '21:30',
                artists: ['Joseph Capriati'],
                separator: ''
              },
              {
                stage: 'Warehouse',
                start: '18:30',
                end: '20:00',
                artists: ['Marco Carola'],
                separator: ''
              },
              {
                stage: 'Warehouse',
                start: '17:00',
                end: '18:30',
                artists: ['Helena Hauff'],
                separator: ''
              },
              {
                stage: 'Warehouse',
                start: '15:30',
                end: '17:00',
                artists: ['Joey Daniel'],
                separator: ''
              },
              {
                stage: 'Warehouse',
                start: '14:00',
                end: '15:30',
                artists: ['Hector'],
                separator: ''
              },
              {
                stage: 'Warehouse',
                start: '12:00',
                end: '14:00',
                artists: ['Jemmy'],
                separator: ''
              },
              {
                stage: 'MK Area 10',
                start: '21:30',
                end: '23:00',
                artists: ['Gorgon City'],
                separator: ''
              },
              {
                stage: 'MK Area 10',
                start: '19:30',
                end: '21:30',
                artists: ['MK'],
                separator: ''
              },
              {
                stage: 'MK Area 10',
                start: '18:00',
                end: '19:30',
                artists: ['Sonny Fodera'],
                separator: ''
              },
              {
                stage: 'MK Area 10',
                start: '16:30',
                end: '18:00',
                artists: ['Weiss'],
                separator: ''
              },
              {
                stage: 'MK Area 10',
                start: '15:00',
                end: '16:30',
                artists: ['Mason Collective'],
                separator: ''
              },
              {
                stage: 'MK Area 10',
                start: '13:30',
                end: '15:00',
                artists: ['Nightlapse'],
                separator: ''
              },
              {
                stage: 'MK Area 10',
                start: '12:00',
                end: '13:30',
                artists: ['Adam Cartwright & Bernie Lee'],
                separator: ''
              },
              {
                stage: 'Sub Aural',
                start: '21:00',
                end: '23:00',
                artists: ['Danny Howard'],
                separator: ''
              },
              {
                stage: 'Sub Aural',
                start: '19:30',
                end: '21:00',
                artists: ['San Divine'],
                separator: ''
              },
              {
                stage: 'Sub Aural',
                start: '18:00',
                end: '19:30',
                artists: ['Hannah Wants'],
                separator: ''
              },
              {
                stage: 'Sub Aural',
                start: '16:00',
                end: '18:00',
                artists: ['Low Steppa'],
                separator: ''
              },
              {
                stage: 'Sub Aural',
                start: '14:30',
                end: '16:00',
                artists: ['Anton Powers'],
                separator: ''
              },
              {
                stage: 'Sub Aural',
                start: '13:00',
                end: '14:30',
                artists: ['Batisse'],
                separator: ''
              },
              {
                stage: 'Sub Aural',
                start: '12:00',
                end: '13:00',
                artists: ['Sean Hughes'],
                separator: ''
              },
              {
                stage: 'Pepsi Max Arena',
                start: '21:30',
                end: '23:00',
                artists: ['Solarstone'],
                separator: ''
              },
              {
                stage: 'Pepsi Max Arena',
                start: '20:00',
                end: '21:30',
                artists: ['Cosmic Gate'],
                separator: ''
              },
              {
                stage: 'Pepsi Max Arena',
                start: '18:30',
                end: '20:00',
                artists: ['Paul Van Dyk'],
                separator: ''
              },
              {
                stage: 'Pepsi Max Arena',
                start: '17:00',
                end: '18:30',
                artists: ['Aly & Fila'],
                separator: ''
              },
              {
                stage: 'Pepsi Max Arena',
                start: '16:00',
                end: '17:00',
                artists: ['Guiseppe Ottaviani'],
                separator: ''
              },
              {
                stage: 'Pepsi Max Arena',
                start: '14:45',
                end: '16:00',
                artists: ['Craig Connelly'],
                separator: ''
              },
              {
                stage: 'Pepsi Max Arena',
                start: '13:30',
                end: '14:45',
                artists: ['Paul Thomas'],
                separator: ''
              },
              {
                stage: 'Pepsi Max Arena',
                start: '12:00',
                end: '13:30',
                artists: ['Rob Harnetty'],
                separator: ''
              },
              {
                stage: 'Silo',
                start: '22:00',
                end: '23:00',
                artists: ['George Kafetzis & Rue Jay'],
                separator: ''
              },
              {
                stage: 'Silo',
                start: '20:30',
                end: '22:00',
                artists: ['Paul Bleasdale'],
                separator: ''
              },
              {
                stage: 'Silo',
                start: '19:00',
                end: '20:30',
                artists: ['K-Klass'],
                separator: ''
              },
              {
                stage: 'Silo',
                start: '18:00',
                end: '19:00',
                artists: ['X-Press 2'],
                separator: ''
              },
              {
                stage: 'Silo',
                start: '16:30',
                end: '18:00',
                artists: ['Tall Paul'],
                separator: ''
              },
              {
                stage: 'Silo',
                start: '15:00',
                end: '15:30',
                artists: ['Seb Fontaine'],
                separator: ''
              },
              {
                stage: 'Silo',
                start: '14:00',
                end: '15:00',
                artists: ['Stuart Hodson & Samuel Lamont'],
                separator: ''
              },
              {
                stage: 'Silo',
                start: '13:00',
                end: '14:00',
                artists: ['Ricco'],
                separator: ''
              },
              {
                stage: 'Silo',
                start: '12:00',
                end: '13:00',
                artists: ['Andy Carroll'],
                separator: ''
              },
              {
                stage: 'The Courtyard',
                start: '22:00',
                end: '23:00',
                artists: ['Joey Riot'],
                separator: ''
              },
              {
                stage: 'The Courtyard',
                start: '20:45',
                end: '22:00',
                artists: ['Coone'],
                separator: ''
              },
              {
                stage: 'The Courtyard',
                start: '19:30',
                end: '20:45',
                artists: ['Kutski'],
                separator: ''
              },
              {
                stage: 'The Courtyard',
                start: '18:30',
                end: '19:30',
                artists: ['Church Service'],
                separator: ''
              },
              {
                stage: 'The Courtyard',
                start: '17:30',
                end: '18:30',
                artists: ['Darren Styles'],
                separator: ''
              },
              {
                stage: 'The Courtyard',
                start: '16:30',
                end: '17:30',
                artists: ['MKN'],
                separator: ''
              },
              {
                stage: 'The Courtyard',
                start: '15:30',
                end: '16:30',
                artists: ['Cally'],
                separator: ''
              },
              {
                stage: 'The Courtyard',
                start: '14:30',
                end: '15:30',
                artists: ['Crystal Mad'],
                separator: ''
              },
              {
                stage: 'The Courtyard',
                start: '13:30',
                end: '14:30',
                artists: ['Callum Highboy'],
                separator: ''
              },
              {
                stage: 'The Courtyard',
                start: '12:00',
                end: '13:30',
                artists: ['Obsession'],
                separator: ''
              },
              {
                stage: 'Utilita Power Tree',
                start: '22:00',
                end: '23:00',
                artists: ['Arielle Free'],
                separator: ''
              },
              {
                stage: 'Utilita Power Tree',
                start: '20:30',
                end: '22:00',
                artists: ['Lilrockit'],
                separator: ''
              },
              {
                stage: 'Utilita Power Tree',
                start: '19:30',
                end: '20:30',
                artists: ['Hannah Laing'],
                separator: ''
              },
              {
                stage: 'Utilita Power Tree',
                start: '18:00',
                end: '19:30',
                artists: ['Barely Legal'],
                separator: ''
              },
              {
                stage: 'Utilita Power Tree',
                start: '16:30',
                end: '18:00',
                artists: ['Vicki Scott'],
                separator: ''
              },
              {
                stage: 'Utilita Power Tree',
                start: '15:00',
                end: '16:30',
                artists: ['Billie Clements'],
                separator: ''
              },
              {
                stage: 'Utilita Power Tree',
                start: '13:30',
                end: '15:00',
                artists: ['DJ Emma'],
                separator: ''
              },
              {
                stage: 'Utilita Power Tree',
                start: '12:00',
                end: '13:30',
                artists: ['Hollie Proffit'],
                separator: ''
              }
            ]
          }
        ]
      }
    ]
  }

  return s
}

// =================================================
// Mutations
// =================================================
export const mutations = {
  TOGGLE_OPTION: (state, { key }) => {
    state.options[key] = !state.options[key]
  },
  SWITCH_VIEW: (state, { key }) => {
    state.currentView = key
  },
  TOGGLE_FAVOURITE: (state, { event }) => {
    event.isFavourite = !event.isFavourite

    // set a color if it has not been set before
    if (!event.color1) {
      event.color1 = Math.random() * 75 - 10
      event.color2 = Math.random() * 75 - 10
      // 0 - 359
    }

    // force refresh of the view
    state.options.favourites = !state.options.favourites
    state.options.favourites = !state.options.favourites
  }
}

// =================================================
// Actions
// =================================================
export const actions = {
  TOGGLE_OPTION({ commit, state }, { key }) {
    commit('TOGGLE_OPTION', { key })
  },
  SWITCH_VIEW({ commit, state }, { key }) {
    commit('SWITCH_VIEW', { key })
  },
  TOGGLE_FAVOURITE({ commit, state, getters }, { day, start, stage }) {
    const event = getters.getEvent(day, start, stage)
    commit('TOGGLE_FAVOURITE', { event })
  }
}

export const getters = {
  getEvent: (state) => (day, start, stage) => {
    return state.festivals[0].timetable
      .filter((timetable) => timetable.day === day)[0]
      .events.filter(
        (event) => event.start === start && event.stage === stage
      )[0]
  }
}
